package br.com.taironnematos.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Classe do tipo Conta com todos seus atributos e elementos relacionados ao JPA/Hibernate.
 * � feito todo devido relacionamento entre as entidades do banco de dados.
 * @author Taironne Matos
 *
 */

@Entity
public class Conta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idConta;
	
	@OneToOne
	@JsonIgnore
	private Pessoa pessoa;
	
	@OneToMany(mappedBy = "conta", orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonIgnore
	private List<Transacao> transacoes = new ArrayList<Transacao>();	

	private Double saldo;
	private Double limiteSaqueDiario;
	private boolean flagAtivo;
	private int tipoConta;
	
	@Temporal(TemporalType.DATE)
	private Date dataCriacao = new java.sql.Date(System.currentTimeMillis());
	
	public Long getIdConta() {
		return idConta;
	}
	
	public void setIdConta(Long idConta) {
		this.idConta = idConta;
	}
	
	public Double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	public Double getLimiteSaqueDiario() {
		return limiteSaqueDiario;
	}
	
	public void setLimiteSaqueDiario(Double limiteSaqueDiario) {
		this.limiteSaqueDiario = limiteSaqueDiario;
	}
	
	public boolean isFlagAtivo() {
		return flagAtivo;
	}
	
	public void setFlagAtivo(boolean flagAtivo) {
		this.flagAtivo = flagAtivo;
	}
	
	public int getTipoConta() {
		return tipoConta;
	}
	
	public void setTipoConta(int tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	public Date getDataCriacao() {
		return dataCriacao;
	}
	
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	public List<Transacao> getTransacoes() {
		return transacoes;
	}

	public void setTransacoes(List<Transacao> contas) {
		this.transacoes = contas;
	}
	
}
