package br.com.taironnematos.domain;

import java.util.Date;

/**
 * Classe axiliar para consulta dos extratos
 * @author Taironne Matos
 *
 */
public class TransacaoEx {
	
	private Long idTransacao;	
	private double valor;
	private char tipo;
	private double valorSaldo;
	private Long idConta;	
	private Date dataTransacao;
	
	public Long getIdTransacao() {
		return idTransacao;
	}
	
	public void setIdTransacao(Long idTransacao) {
		this.idTransacao = idTransacao;
	}
	
	public double getValor() {
		return valor;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	public char getTipo() {
		return tipo;
	}
	
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	
	public double getValorSaldo() {
		return valorSaldo;
	}
	
	public void setValorSaldo(double valorSaldo) {
		this.valorSaldo = valorSaldo;
	}	
	
	public Long getIdConta() {
		return idConta;
	}

	public void setIdConta(Long idConta) {
		this.idConta = idConta;
	}

	public Date getDataTransacao() {
		return dataTransacao;
	}
	
	public void setDataTransacao(Date dataTransacao) {
		this.dataTransacao = dataTransacao;
	}

}
