package br.com.taironnematos.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Classe do tipo Transa��o com todos seus atributos e elementos relacionados ao JPA/Hibernate.
 * � feito todo devido relacionamento entre as entidades do banco de dados.
 * @author Taironne Matos
 *
 */
	
@Entity
public class Transacao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTransacao;
	
	private double valor;
	private char tipo;
	private double valorSaldo;

	@OneToOne
	private Conta conta;
	
	@Temporal(TemporalType.DATE)
	private Date dataTransacao = new java.sql.Date(System.currentTimeMillis());
	
	public Long getIdTransacao() {
		return idTransacao;
	}
	
	public void setIdTransacao(Long idTransacao) {
		this.idTransacao = idTransacao;
	}
	
	public double getValor() {
		return valor;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	public Date getDataTransacao() {
		return dataTransacao;
	}
	
	public void setDataTransacao(Date dataTransacao) {
		this.dataTransacao = dataTransacao;
	}

	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public double getValorSaldo() {
		return valorSaldo;
	}

	public void setValorSaldo(double valorSaldo) {
		this.valorSaldo = valorSaldo;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}
}
