package br.com.taironnematos.domain;

/**
 * Classe que cont�m o c�digo e a mensagem de erro que a API devolver� para o Cliente.
 * @author Taironne Matos
 * 
 */
public class ErrorMessage {
	
	private String message;
	private int code;
	
	public ErrorMessage(String message, int code) {
		super();
		this.message = message;
		this.code = code;
	}

	public ErrorMessage() {
		super();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}	

}
