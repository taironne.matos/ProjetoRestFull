package br.com.taironnematos.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.taironnematos.domain.Conta;
import br.com.taironnematos.services.ContaService;

/**
 * Classe que cont�m os web services relacionados a requisi��es da entidade Conta.
 * @author Taironne Matos
 *
 */
@Path("/conta")
public class ContaResource {
	
	private static final String CHARSET_UTF8 = ";charsert=utf-8";
	ContaService service = new ContaService();
	
	/**
	 * M�todo que cria uma conta e a relaciona a uma pessoa com ID informado na URL.
	 * @param conta
	 * @param idPessoa
	 * @return
	 */
	@POST
	@Path("{idPessoa}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
    public Response save(Conta conta, @PathParam("idPessoa") long idPessoa) {
        conta = service.save(conta, idPessoa);
        return Response.status(Status.CREATED).entity(conta).build();
    }	
	
	/**
	 * M�todo que deleta uma conta de ID informado na URL.
	 * @param idConta
	 */
	@DELETE
	@Path("{idConta}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public void delete(@PathParam("idConta") long idConta) {
		service.delete(idConta);
	}
	
	/**
	 * M�todo que consulta o saldo de ID informado na URL.
	 * @param idConta
	 */
	@GET
	@Path("{idConta}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.TEXT_PLAIN + CHARSET_UTF8)
	public String saldo(@PathParam("idConta") long idConta) {
		return service.saldo(idConta);		
	}
	
	/**
	 * M�todo que bloqueia uma conta de ID informado na URL.
	 * @param idConta
	 * @return
	 */
	
	@PATCH
	@Path("{idConta}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response bloquearConta(@PathParam("idConta") long idConta) {
		Conta conta = service.bloquearConta(idConta);
		return Response.status(Status.CREATED).entity(conta).build();		
	}
	
	/**
	 * M�todo que chama o construtor das web services relacionadas as transa��es. 
	 * @return
	 */
	/*
	@Path("{idConta}/transacao")
    public TransacaoResource getTransacaoResource() {
        return new TransacaoResource();
    }
	*/
	/*
	@PUT
	@Path("{idPessoa}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public String update(@PathParam("idPessoa") long idPessoa, Conta contaUpdate) {
		contaUpdate.getPessoa().setIdPessoa(idPessoa);
        return service.update(contaUpdate);
    }
    */

	
	
}
