package br.com.taironnematos.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.taironnematos.domain.Pessoa;
import br.com.taironnematos.services.PessoaService;

/**
 * Classe que cont�m os web services relacionados a requisi��es da entidade Pessoa.
 * @author Taironne Matos
 *
 */
@Path("/pessoa")
public class PessoaResource {
	
	private static final String CHARSET_UTF8 = ";charset=utf-8";
	
	PessoaService service = new PessoaService();
	
	/**
	 * M�todo que cria uma pessoa.
	 * @param pessoa
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON)
    public Response save(Pessoa pessoa) {
		pessoa = service.save(pessoa);
        return Response.status(Status.CREATED).entity(pessoa).build();
    }	
	
	/**
	 * M�todo que edita uma pessoa.
	 * @param idPessoa
	 * @param pessoaUpdate
	 */
	@PUT
	@Path("{idPessoa}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
    public void update(@PathParam("idPessoa") long idPessoa, Pessoa pessoaUpdate) {
		pessoaUpdate.setIdPessoa(idPessoa);
        pessoaUpdate = service.update(pessoaUpdate);
    }
	
	/**
	 * M�todo que deleta uma pessoa pelo ID informado na URL.
	 * @param idPessoa
	 */
	@DELETE
	@Path("{idPessoa}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public void delete(@PathParam("idPessoa") long idPessoa) {
		service.delete(idPessoa);
	}
	
	/**
	 * M�todo que busca uma pessoa pelo ID informado na URL.
	 * @param idPessoa
	 * @return
	 */
	@GET
	@Path("{idPessoa}")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Pessoa getById(@PathParam("idPessoa") long idPessoa) {
		Pessoa pessoa = service.getById(idPessoa);
		return pessoa;
	}
	
	/*@Path("{idPessoa}/conta")
    public ContaResource getContaResource() {
        return new ContaResource();
    }*/
	
}
