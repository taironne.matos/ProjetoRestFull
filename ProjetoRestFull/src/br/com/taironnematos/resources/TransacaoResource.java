package br.com.taironnematos.resources;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.taironnematos.domain.Transacao;
import br.com.taironnematos.domain.TransacaoEx;
import br.com.taironnematos.services.TransacaoService;
/**
 * Classe que cont�m os web services relacionados a requisi��es da entidade Transa��o.
 * @author Taironne Matos
 *
 */
/*
 * N�o foi definido um Path para Transa��o, pois obrigatoriamente uma opera��o n�o � realizada sem uma conta relacionada.
 */
@Path("/transacao")
public class TransacaoResource {
			
	private static final String CHARSET_UTF8 = ";charsert=utf-8";
	TransacaoService service = new TransacaoService();
	
	/**
	 * M�todo que realiza as opera��es de saque e dep�sito. � pessado no JSON o tipo de transa��o que deseja efetuar.
	 * @param transacao
	 * @param idConta
	 * @return
	 */
	@POST
	@Path("{idConta}")	
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON)
    public Response operation(Transacao transacao, @PathParam("idConta") long idConta) {
		transacao = service.operation(transacao, idConta);
		return Response.status(Status.CREATED).entity(transacao).build();
    }	
	
	/**
	 * M�todo que gera extrato por conta.
	 * @param idConta
	 * @return
	 */	
	@GET
	@Path("{idConta}")	
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON)
	public List<TransacaoEx> extrato(@PathParam("idConta") long idConta) {
		return service.extrato(idConta);
	}	
	
	
//	@POST
//	@Path("/extrato/data/{idConta}")
//	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
//	@Produces(MediaType.APPLICATION_JSON)
//	public List<TransacaoEx> extratoPorPeriodo(@PathParam("idConta") long idConta, Date dataInicial, Date dataFinal) {		
//		return service.extratoPorData(idConta, dataInicial, dataInicial);		
//	}
}
	
