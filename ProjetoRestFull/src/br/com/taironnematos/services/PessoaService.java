package br.com.taironnematos.services;

import br.com.taironnematos.dao.PessoaDAO;
import br.com.taironnematos.domain.Pessoa;

/**
 * Classe � um subrecurso da classe das implementa��es dos webservices. Foi criada com finalidade de facilitar manuten��es futuras.
 * @author Taironne Matos
 *
 */
public class PessoaService {
	
	PessoaDAO dao = new PessoaDAO();
	
	public Pessoa save (Pessoa pessoa) {
		return dao.save(pessoa);
	}	
	
	public Pessoa update (Pessoa pessoaUpdate) {
		return dao.update(pessoaUpdate);
	}
	
	public Pessoa delete (long idPessoa) {
		return dao.delete(idPessoa);
	}
	
	public Pessoa getById (long idPessoa) {
		return dao.getById(idPessoa);
	}
}
