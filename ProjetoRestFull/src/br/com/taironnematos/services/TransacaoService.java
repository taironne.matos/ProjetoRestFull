package br.com.taironnematos.services;

import java.util.Date;
import java.util.List;

import br.com.taironnematos.dao.TransacaoDAO;
import br.com.taironnematos.domain.Transacao;
import br.com.taironnematos.domain.TransacaoEx;

/**
 * Classe � um subrecurso da classe das implementa��es dos webservices. Foi criada com finalidade de facilitar manuten��es futuras.
 * @author Taironne Matos
 *
 */
public class TransacaoService {
	
	TransacaoDAO service = new TransacaoDAO();
	
	public Transacao operation (Transacao transacao, long idConta) {
		return service.operation(transacao, idConta);
	}
		
	public List<TransacaoEx> extrato (long idConta) {
		return service.extrato(idConta);
	}
	
//	public List<TransacaoEx> extratoPorData (long idConta, Date dataInicial, Date dataFinal) {
//		return service.extratoPorData(idConta, dataInicial, dataFinal);
//	}

}
