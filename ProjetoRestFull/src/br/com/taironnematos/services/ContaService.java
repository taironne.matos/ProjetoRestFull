package br.com.taironnematos.services;

import br.com.taironnematos.dao.ContaDAO;
import br.com.taironnematos.domain.Conta;
/**
 * Classe � um subrecurso da classe das implementa��es dos webservices. Foi criada com finalidade de facilitar manuten��es futuras. 
 * @author Taironne Matos
 *
 */
public class ContaService {
	
	ContaDAO service = new ContaDAO();
	
	public Conta save (Conta conta, long idPessoa) {
		return service.save(conta, idPessoa);
	}	
	
	public Conta delete (long idConta) {
		return service.delete(idConta);
	}
	
	public Conta bloquearConta (long idConta){
		return service.bloquearConta(idConta);
	}
	
	public String saldo (long idConta) {
		return service.saldo(idConta);
	}
}
