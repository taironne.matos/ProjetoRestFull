package br.com.taironnematos.exceptions;

/**
 * Classe que cont�m os erros gerados pela aplica��o.
 * @author Taironne Matos
 *
 */
public enum ErrorCode {
	
	/*
	 * Erro nas requisi��es.
	 */
	BAD_REQUEST(400),
	/*
	 * Erro na busca no banco de dados.
	 */
	NOT_FOUND(404),
	/*
	 * Erro relacionados ao servidor.
	 */
	SERVER_ERROR(500); 
	
	private int code;
	
	ErrorCode(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}

}
