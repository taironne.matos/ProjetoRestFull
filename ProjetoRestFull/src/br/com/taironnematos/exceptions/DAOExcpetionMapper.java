package br.com.taironnematos.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.taironnematos.domain.ErrorMessage;

/**
 * Classe que trata as exe��es lan�adas no c�digo.
 * @author Taironne Matos
 *
 */
@Provider
public class DAOExcpetionMapper implements ExceptionMapper<DAOException>{

	@Override
	public Response toResponse(DAOException e) {
		ErrorMessage error = new ErrorMessage(e.getMessage(), e.getCode());
		
		if (e.getCode() == ErrorCode.BAD_REQUEST.getCode()) {
			return Response.status(Status.BAD_REQUEST).entity(error).type(MediaType.APPLICATION_JSON).build();
		}
		
		else if (e.getCode() == ErrorCode.NOT_FOUND.getCode()) {
				return Response.status(Status.NOT_FOUND).entity(error).type(MediaType.APPLICATION_JSON).build();			
		}
		
		else {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).type(MediaType.APPLICATION_JSON).build();
		}
	}

}
