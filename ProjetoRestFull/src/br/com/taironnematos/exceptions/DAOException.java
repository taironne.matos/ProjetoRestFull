package br.com.taironnematos.exceptions;

/**
 * Classe de cria��o da exce��o que ser� lan�ada caso ocorro algo inesperado (DAOException).
 * @author Taironne Matos
 *
 */
public class DAOException extends RuntimeException {
	
	private static final long serialVersionUID = 1436886958541248218L;
	private int code;
	
	public DAOException(String message, int code) {
		super(message);
		this.code = code;
	}

	public int getCode() {
		return code;
	}
	
	
}
