package br.com.taironnematos.dao;

import javax.persistence.EntityManager;

import br.com.taironnematos.domain.Conta;
import br.com.taironnematos.domain.Pessoa;
import br.com.taironnematos.exceptions.DAOException;
import br.com.taironnematos.exceptions.ErrorCode;

/**
 *
 *Classe respons�vel por toda implementa��o dos m�todos relacionados aos Web Services (Conta).
 * 
 * @author Taironne Matos
 * 
 */
public class ContaDAO {	
	
	/**
	 * M�todo que cria uma conta e associa a mesma a uma pessoa.
	 * @param conta
	 * @param idPessoa
	 * @return
	 */
	
	public Conta save(Conta conta, long idPessoa) {
		
		EntityManager conexao = JPAUtil.getEntityManager();
		Pessoa pessoa;		
		
		try {
		conexao.getTransaction().begin();
		pessoa = conexao.find(Pessoa.class, idPessoa);
		conta.setPessoa(pessoa);
		conexao.persist(conta);
		conexao.getTransaction().commit();		
		}
		catch (RuntimeException e) {
			conexao.getTransaction().rollback();
        	throw new DAOException("Erro ao salvar registro. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
		finally {
			conexao.close();
		}
		return conta;
	}	
	
	/**
	 * M�todo que edita uma conta.
	 * @param contaUpdate
	 * @return
	 */
	
	public Conta update(Conta contaUpdate) {
        EntityManager conexao = JPAUtil.getEntityManager();
        Conta conta = null;
        
        if (contaUpdate.getIdConta() <= 0) {
        	throw new DAOException("O ID da conta deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
        
        if (!contaIsValid(contaUpdate)) {
			throw new DAOException("Dados incompletos. ", ErrorCode.BAD_REQUEST.getCode());
		}
        
        try {
	        conexao.getTransaction().begin();
	        conta = conexao.find(Conta.class, contaUpdate.getIdConta());
	        conta.setSaldo(contaUpdate.getSaldo());
	        conta.setTipoConta(contaUpdate.getTipoConta());
	        conta.setFlagAtivo(contaUpdate.isFlagAtivo());
	        conta.setDataCriacao(contaUpdate.getDataCriacao());
	        conta.setLimiteSaqueDiario(contaUpdate.getLimiteSaqueDiario());
	        conexao.getTransaction().commit();        
        }
        catch (NullPointerException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Conta para atualiza��o n�o existe no banco de dados. " 
        			+ e.getMessage(), ErrorCode.NOT_FOUND.getCode());
        }
        catch (RuntimeException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Erro na atualiza��o dos registros. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
        finally {
			conexao.close();
		}
        
        return conta;
    }
	
	/**
	 * M�todo que deleta uma conta pelo seu ID.
	 * @param idConta
	 * @return
	 */
	
	public Conta delete(Long idConta) {
        EntityManager conexao = JPAUtil.getEntityManager();
        Conta conta = null;
        
        if (idConta <= 0) {
        	throw new DAOException("O ID da conta deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
        
        try {
	        conexao.getTransaction().begin();
	        conta = conexao.find(Conta.class, idConta);
	        conexao.remove(conta);
	        conexao.getTransaction().commit();	       
        }
        catch (NullPointerException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Conta para exclus�o n�o existe no banco de dados. " 
        			+ e.getMessage(), ErrorCode.NOT_FOUND.getCode());
        }
        catch (RuntimeException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Erro ao remover os registros. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
        finally {
			conexao.close();
		}
        
        return conta;
	}    
	
	/**
	 * M�todo que bloqueia uma conta pelo seu ID.
	 * @param idConta
	 * @return
	 */
	
	public Conta bloquearConta(long idConta) {
		EntityManager conexao = JPAUtil.getEntityManager();
		Conta conta = null;
		
	    if (idConta <= 0) {
        	throw new DAOException("O ID da conta deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
	
		try {			
			conta = conexao.find(Conta.class, idConta);			
			conta.setFlagAtivo(false);				
		}
		catch (RuntimeException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Erro ao bloquear conta. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
		finally {
			conexao.close();
		}
		
		return conta;
	}
	
	/**
	 * M�todo que consulta o saldo de uma conta pelo seu ID.
	 * @param idConta
	 * @return
	 */
	
	public String saldo(long idConta) {
		EntityManager conexao = JPAUtil.getEntityManager();
		String msg;
		
		if (idConta <= 0) {
        	throw new DAOException("O ID da conta deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
		try {
			Conta conta = conexao.find(Conta.class, idConta);
			msg = ("Cliente: " + conta.getPessoa().getNome() + " ---" +" Saldo da Conta: R$" + conta.getSaldo());
		}
		catch (RuntimeException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Erro na consulta dos registros. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
		finally {
			conexao.close();
		}
		return msg;
	}
	
	
	
	/**
	 * M�todo que valida a exist�ncia de uma conta. Esse m�todo � ultizado nos m�todos de salvar e editar contas.
	 * @param conta
	 * @return
	 */
	
	private boolean contaIsValid(Conta conta) {
		
		try {
			if (conta.getPessoa().getNome().isEmpty()) {
				return false;
			}
		}
		catch (NullPointerException e) {
        	throw new DAOException("Dados incompletos. " + e.getMessage(), ErrorCode.NOT_FOUND.getCode());
        }
		
		return true;
	}
	

}
