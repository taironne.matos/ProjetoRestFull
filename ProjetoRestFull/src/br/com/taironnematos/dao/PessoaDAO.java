package br.com.taironnematos.dao;

import javax.persistence.EntityManager;

import br.com.taironnematos.domain.Pessoa;
import br.com.taironnematos.exceptions.DAOException;
import br.com.taironnematos.exceptions.ErrorCode;

/**
*
*Classe respons�vel por toda implementa��o dos m�todos relacionados aos Web Services (Pessoa).
* 
* @author Taironne Matos
* 
*/

public class PessoaDAO {
	
	/**
	 * M�todo que cria uma pessoa.
	 * @param pessoa
	 * @return
	 */
	public Pessoa save(Pessoa pessoa) {
		
		EntityManager conexao = JPAUtil.getEntityManager();		
		
		try {
			conexao.getTransaction().begin();
			conexao.persist(pessoa);
			conexao.getTransaction().commit();		
		}
		catch (RuntimeException e) {
			conexao.getTransaction().rollback();
        	throw new DAOException("Erro ao salvar registro. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
		finally {
			conexao.close();
		}
		
		return pessoa;
	}
	
	/**
	 * M�todo que retona uma pessoa pelo seu ID.
	 * @param idPessoa
	 * @return
	 */	
	public Pessoa getById(long idPessoa) {
        EntityManager conexao = JPAUtil.getEntityManager();
        Pessoa pessoa = null;
        
        if (idPessoa <= 0) {
        	throw new DAOException("O ID de pessoa deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
        
        try {
        	pessoa = conexao.find(Pessoa.class, idPessoa);
        }
        catch (RuntimeException e) {
        	throw new DAOException("Erro ao salvar registro. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
        finally {
			conexao.close();
		}
        
        if (pessoa == null) {
        	throw new DAOException("Pessoa de ID " + idPessoa + "n�o existe. ", ErrorCode.SERVER_ERROR.getCode());
        }
        
        return pessoa;
    }
	
	/**
	 * M�todo que edita uma pessoa.
	 * @param pessoaUpdate
	 * @return
	 */
	public Pessoa update(Pessoa pessoaUpdate) {
        EntityManager conexao = JPAUtil.getEntityManager();
        Pessoa pessoa = null;
                     
        if (pessoaUpdate.getIdPessoa() <= 0) {
        	throw new DAOException("O ID de pessoa deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
        
        try {
	        conexao.getTransaction().begin();
	        pessoa = conexao.find(Pessoa.class, pessoaUpdate.getIdPessoa());
	        pessoa.setCpf(pessoaUpdate.getCpf());
	        pessoa.setDataNascimento(pessoaUpdate.getDataNascimento());
	        pessoa.setNome(pessoaUpdate.getNome());
	        conexao.getTransaction().commit();
        }
        catch (NullPointerException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Pessoa para atualiza��o n�o existe no banco de dados. " 
        			+ e.getMessage(), ErrorCode.NOT_FOUND.getCode());
        }
        catch (RuntimeException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Erro na atualiza��o dos registros. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
        finally {
			conexao.close();
		}
       
        return pessoa;
    }	
	
	/**
	 * M�todo que deleta uma pessoa do banco de dados.
	 * @param idPessoa
	 * @return
	 */
	public Pessoa delete(Long idPessoa) {
        EntityManager conexao = JPAUtil.getEntityManager();
        Pessoa pessoa = null;
        
        if (idPessoa <= 0) {
        	throw new DAOException("O ID de pessoa deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
        
        try {
	        conexao.getTransaction().begin();
	        pessoa = conexao.find(Pessoa.class, idPessoa);
	        conexao.remove(pessoa);
	        conexao.getTransaction().commit();
        }
        catch (NullPointerException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Pessoa para exclus�o n�o existe no banco de dados. " 
        			+ e.getMessage(), ErrorCode.NOT_FOUND.getCode());
        }
        catch (RuntimeException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Erro ao remover os registros. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
        finally {
			conexao.close();
		}
        
        return pessoa;
    }    

}
