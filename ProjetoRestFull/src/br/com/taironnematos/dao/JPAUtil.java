package br.com.taironnematos.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Classe respons�vel pelo gerenciamento de conex�o com o banco de dados.
 * @author Taironne Matos
 *
 */

public class JPAUtil {
	
	private static EntityManagerFactory emf;
		
	public static EntityManager getEntityManager() {
		 if (emf == null) {
			emf = Persistence.createEntityManagerFactory("projeto-java");
		 }
		 return emf.createEntityManager();
	}

}
