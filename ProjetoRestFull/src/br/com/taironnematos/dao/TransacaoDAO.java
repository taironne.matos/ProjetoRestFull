package br.com.taironnematos.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.taironnematos.domain.Conta;
import br.com.taironnematos.domain.Transacao;
import br.com.taironnematos.domain.TransacaoEx;
import br.com.taironnematos.exceptions.DAOException;
import br.com.taironnematos.exceptions.ErrorCode;

/**
 * Classe respons�vel por toda implementa��o dos m�todos relacionados aos Web Services (Transa��o).
 * @author Taironne Matos
 *
 */
public class TransacaoDAO {
	
	/**
	 * M�todo que realiza as opera��es de saque e dep�sito. 
	 * @param transacao
	 * @param idConta
	 * @return
	 */
	public Transacao operation(Transacao transacao, long idConta) {
		
		EntityManager conexao = JPAUtil.getEntityManager();
		Conta conta;
		
		if (transacao.getValor() <= 0) {
        	throw new DAOException("O ID da conta deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
		
		if (idConta <= 0) {
        	throw new DAOException("O ID da conta deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
		
		try {
			conexao.getTransaction().begin();
			conta = conexao.find(Conta.class, idConta);
			
			if (transacao.getTipo() == 's' || transacao.getTipo() == 'S') {
				if (conta.isFlagAtivo() == true) {	
					if ((transacao.getValor() <= conta.getSaldo()) && (transacao.getValor() <= conta.getLimiteSaqueDiario()) ) {
						 conta.setSaldo(conta.getSaldo() - transacao.getValor());	
						 transacao.setValorSaldo(conta.getSaldo());
					}					
				}
			}
			
			else if ((transacao.getTipo() == 'd' || transacao.getTipo() == 'D')) {
				if (conta.isFlagAtivo() == true) {
					if (transacao.getValor() > 0) {
						 conta.setSaldo(conta.getSaldo() + transacao.getValor());
						 transacao.setValorSaldo(conta.getSaldo());
					}
				}	
			}		
			transacao.setConta(conta);
			conexao.persist(transacao);
			conexao.getTransaction().commit();
		}
		catch (RuntimeException e) {
        	conexao.getTransaction().rollback();
        	throw new DAOException("Erro na opera��o. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
		finally {
			conexao.close();
		}
		
		return transacao;
	}
	
	/**
	 * M�todo que realiza a opera��o de extrato por conta.
	 * @param idConta
	 * @return
	 */
	public List<TransacaoEx> extrato(long idConta) {
	    EntityManager conexao = JPAUtil.getEntityManager();	
	    Conta conta = conexao.find(Conta.class, idConta);
	    List<Transacao> transacoes = new ArrayList<Transacao>();
	    List<TransacaoEx> transacoesEx = new ArrayList<TransacaoEx>();
	    
	    if (idConta <= 0) {
        	throw new DAOException("O ID da conta deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
	    
	    try {
	    	transacoes = conta.getTransacoes();
	    	for (Transacao t : transacoes) {
	    		TransacaoEx tranEx = new TransacaoEx();
	    		
	    		tranEx.setIdConta(conta.getIdConta());
	    		tranEx.setIdTransacao(t.getIdTransacao());
	    		tranEx.setValor(t.getValor());
	    		tranEx.setValorSaldo(t.getValorSaldo());
	    		tranEx.setTipo(t.getTipo());
	    		tranEx.setDataTransacao(t.getDataTransacao());	  
	    		
	    		transacoesEx.add(tranEx);
	    	}
	    }
	    catch (RuntimeException e) {        	
        	throw new DAOException("Erro na consulta dos registros. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
	    finally {
			conexao.close();
		}
	    return transacoesEx;
	}
	
	/**
	 * M�todo que realiza a opera��o de extrato por per�dodo por conta .
	 * @param idConta
	 * @return
	 */
	
	public List<TransacaoEx> extratoPorData(long idConta, Date dataInicial, Date dataFinal) {
	    EntityManager conexao = JPAUtil.getEntityManager();	
	    Conta conta = conexao.find(Conta.class, idConta);
	    List<Transacao> transacoes = new ArrayList<Transacao>();
	    List<TransacaoEx> transacoesEx = new ArrayList<TransacaoEx>();
	    
	    if (idConta <= 0) {
        	throw new DAOException("O ID da conta deve ser maior que 0. ", ErrorCode.BAD_REQUEST.getCode());
        }
	    
	    try {
	    	transacoes = conta.getTransacoes();
	    	for (Transacao t : transacoes) {
	    		TransacaoEx tranEx = new TransacaoEx();
	    		Date data = conta.getDataCriacao();
	    		
	    		if (isValideDate(data, dataInicial, dataFinal)) {
	    			tranEx.setIdConta(conta.getIdConta());
		    		tranEx.setIdTransacao(t.getIdTransacao());
		    		tranEx.setValor(t.getValor());
		    		tranEx.setValorSaldo(t.getValorSaldo());
		    		tranEx.setTipo(t.getTipo());
		    		tranEx.setDataTransacao(t.getDataTransacao());	  
		    		
		    		transacoesEx.add(tranEx);
	    		}	    		
	    	}
	    }
	    catch (RuntimeException e) {        	
        	throw new DAOException("Erro na consulta dos registros. " + e.getMessage(), ErrorCode.SERVER_ERROR.getCode());
        }
	    finally {
			conexao.close();
		}
	    return transacoesEx;
	}
	
	/**
	 * M�todo que valida
	 * @param dataInicial
	 * @param dataFinal
	 * @return
	 */
	public boolean isValideDate (Date dataConsulta ,Date dataInicial, Date dataFinal) {
		boolean data;
		
		if ((dataConsulta.before(dataFinal)) && (dataConsulta.after(dataInicial))) {
			data = true;
		}		
		else {
			data = false;
		}
		
		return data;
	}

}














